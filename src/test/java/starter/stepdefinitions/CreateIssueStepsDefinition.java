package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import nl.abnamro.api.CreateIssueApi;
import nl.abnamro.model.create.Issue;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateIssueStepsDefinition {

    private static final Logger logger = LoggerFactory.getLogger(CreateIssueStepsDefinition.class);

    @Given("random existing issue")
    public void randomExistingIssue() {

        logger.info("Creating random issue");
        String title = RandomStringUtils.randomAlphabetic(20);
        Serenity.setSessionVariable("title").to(title);

        CreateIssueApi api = new CreateIssueApi();
        Issue createdIssue = api.createNewIssue(title);

        Serenity.setSessionVariable("iid").to(createdIssue.getIid());
        Serenity.setSessionVariable("id").to(createdIssue.getId());
        logger.info("Issue successfully created ");
    }

    @When("issue with title {string} is created")
    public void randomWithIsCreated(String title) {

        logger.info(String.format("Creating issue with title: %s", title));
        Serenity.setSessionVariable("title").to(title);

        CreateIssueApi api = new CreateIssueApi();
        Issue createdIssue = api.createNewIssue(title);

        Assert.assertEquals("Title in created issue differs from the requested one",
                createdIssue.getTitle(), title);

        Serenity.setSessionVariable("iid").to(createdIssue.getIid());

        logger.info("Issue successfully created ");
    }


    @When("attempt to create issue without title")
    public void attemptToCreateIssueWithoutTitle() {

        logger.info("Creating issue without title");

        CreateIssueApi api = new CreateIssueApi();
        Response response = api.createIssueWithoutTitle();

        Serenity.setSessionVariable("response").to(response);
    }

    @When("attempt to create issue without authentication")
    public void attemptToCreateIssueWithoutAuthentication() {

        logger.info("Creating issue without authentication");

        CreateIssueApi api = new CreateIssueApi();
        Response response = api.createIssueWithoutAuth();

        Serenity.setSessionVariable("response").to(response);
    }


    @Then("Error code is {string}")
    public void verifyErrorCode(String errorCode) {

        Response response = Serenity.sessionVariableCalled("response");

        Integer expected = Integer.valueOf(errorCode);
        Integer actual = response.getStatusCode();
        Assert.assertEquals(String.format("Expected error code %s does not match actual one %d", expected, actual),
                expected, actual);

    }

    @Then("error message is {string}")
    public void errorMessageIs(String expectedMessage) {

        Response response = Serenity.sessionVariableCalled("response");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String actualErrorMessage = jsonPathEvaluator.get("message").toString();

        Assert.assertTrue(String.format("Expected error message: %s is different from actual: %s", expectedMessage, actualErrorMessage),
                actualErrorMessage.contains(expectedMessage));
    }


}
