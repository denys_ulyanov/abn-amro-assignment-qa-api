package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import net.serenitybdd.core.Serenity;
import nl.abnamro.api.FindIssueApi;
import nl.abnamro.model.search.FoundIssue;
import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class SearchIssueStepsDefinition {

    private static final Logger logger = LoggerFactory.getLogger(SearchIssueStepsDefinition.class);


    @Then("issue can be found by title")
    public void issueCanBeFoundByTitle() {

        String title = Serenity.sessionVariableCalled("title");

        logger.info(String.format("Searching for issue with title: %s", title));

        SoftAssertions softAssertion = new SoftAssertions();

        FindIssueApi findIssueApi = new FindIssueApi();
        List<FoundIssue> issues = findIssueApi.findIssueByTitle(title);

        for (FoundIssue issue : issues) {
            softAssertion.assertThat(issue.getTitle())
                    .as("Validate title is %s", title)
                    .containsIgnoringCase(title);
        }

        logger.info("Issue found successfully");

    }


    @Then("issue can be found by IID")
    public void issueCanBeFoundByIID() {

        int iid = Serenity.sessionVariableCalled("iid");
        int id = Serenity.sessionVariableCalled("id");

        logger.info(String.format("Searching for issue with iid: %d", iid));

        FindIssueApi findIssueApi = new FindIssueApi();
        List<FoundIssue> issues = findIssueApi.findIssueByID(iid);

        Assert.assertEquals("More then one ticket is found by IID",
                1, issues.size());

        FoundIssue issue = issues.get(0);

        Assert.assertEquals("IID does not match in found issue",
                issue.getIid(), Integer.valueOf(iid));

        Assert.assertEquals("ID does not match in found issue",
                issue.getId(), Integer.valueOf(id));
    }

    @Then("issue state is {string}")
    public void issueStatusIs(String expectedState) {

        int iid = Serenity.sessionVariableCalled("iid");

        logger.info(String.format("Searching for issue with iid: %d", iid));

        FindIssueApi findIssueApi = new FindIssueApi();
        List<FoundIssue> issues = findIssueApi.findIssueByID(iid);
        FoundIssue issue = issues.get(0);
        String actualState = issue.getState();

        logger.info("Issue found successfully");

        Assert.assertEquals("Current issue state %s did not match expected %s",
                actualState, expectedState);

        logger.info("Issue state was updated successfully");
    }

    @Then("issue can not be found by IID")
    public void issueCanNotBeFoundByIID() {

        int iid = Serenity.sessionVariableCalled("iid");

        logger.info(String.format("Searching for issue with iid: %d", iid));

        FindIssueApi findIssueApi = new FindIssueApi();
        List<FoundIssue> issues = findIssueApi.findIssueByID(iid);

        Assert.assertEquals("Issue was found when it should not be",
                issues.size(), 0);
    }

}
