package starter.stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import nl.abnamro.api.DeleteIssueApi;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteIssueStepsDefinition {

    private static final Logger logger = LoggerFactory.getLogger(DeleteIssueStepsDefinition.class);

    @When("issue is deleted")
    public void deleteIssueByIID() {

        int iid = Serenity.sessionVariableCalled("iid");

        logger.info(String.format("Delete issue with iid: %d", iid));

        DeleteIssueApi api = new DeleteIssueApi();
        Response response = api.deleteIssue(iid);

        int statusCode = response.getStatusCode();

        Assert.assertEquals(String.format("API response is %d, when expected is 200", statusCode),
                statusCode, 204);
    }


    @After
    public void afterScenario() {
        try {
            DeleteIssueApi api = new DeleteIssueApi();
            api.deleteIssue(Serenity.sessionVariableCalled("iid"));
        } catch (Exception e) {
            logger.info("After scenario failed to delete issue");
        }

    }
}
