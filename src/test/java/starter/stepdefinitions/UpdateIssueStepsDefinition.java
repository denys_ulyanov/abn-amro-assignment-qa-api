package starter.stepdefinitions;

import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import nl.abnamro.api.UpdateIssueApi;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateIssueStepsDefinition {

    private static final Logger logger = LoggerFactory.getLogger(UpdateIssueStepsDefinition.class);

    @When("issue state updated to {string}")
    public void issueStatusUpdatedTo(String state) {

        int iid = Serenity.sessionVariableCalled("iid");

        logger.info(String.format("Changing issue state with iid %d to %s ", iid, state));

        UpdateIssueApi api = new UpdateIssueApi();
        Response response = api.updateIssueState(iid, state);

        Assert.assertEquals("API response is %d, when expected is 200",
                response.getStatusCode(), 200);
    }
}
