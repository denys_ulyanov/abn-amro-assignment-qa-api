Feature: Verify issue can be updated in Issues API

  Scenario: Verify issue status can be changed
    Given random existing issue
    When issue state updated to "close"
    Then issue state is "closed"

