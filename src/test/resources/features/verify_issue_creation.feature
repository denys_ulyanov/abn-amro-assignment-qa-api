Feature: Verify creation of the new issue in Issues API

  Scenario: Verify issue can be created with title only
    When issue with title "This is a test title" is created
    Then issue can be found by title

  Scenario: Verify issue can not be created without title
    When attempt to create issue without title
    Then Error code is "400"
    And error message is "{title=[can't be blank]}"

  Scenario: Verify issue can not be created without authentication
    When attempt to create issue without authentication
    Then Error code is "401"
    And error message is "401 Unauthorized"