Feature: Verify issue can be deleted in Issues API

  Scenario: Verify issue can be deleted by IID
    Given random existing issue
    When issue is deleted
    Then issue can not be found by IID