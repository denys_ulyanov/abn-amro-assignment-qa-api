package nl.abnamro.api;

import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;

import static io.restassured.RestAssured.given;

/**
 * The API representation for Delete issue at Gitlab Issues API
 * <p>
 * This class handles the calls to delete issue endpoint using RestAssured.
 * </p>
 *
 * @author Denys Ulyanov
 */
public class DeleteIssueApi {

    /**
     * Delete issue by IID
     * <p>
     * This method handles the call Delete Issue endpoint
     * </p>
     *
     * @param iid IID of  issue to delete
     * @return the Response object
     * @see <a href="https://docs.gitlab.com/ee/api/issues.html#delete-an-issue">Delete issue</a>
     */
    public Response deleteIssue(Integer iid) {

        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());

        return given().log().all()
                .auth().oauth2(Serenity.environmentVariables().getProperty("token"))
                .baseUri(Serenity.environmentVariables().getProperty("base.url"))
                .pathParam("iid", iid)
                .when()
                .delete("/issues/{iid}");
    }
}
