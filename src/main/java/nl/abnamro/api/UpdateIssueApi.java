package nl.abnamro.api;

import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;

import static io.restassured.RestAssured.given;

/**
 * The API representation for update issue at Gitlab Issues API
 * <p>
 * This class handles the calls to update issue endpoint using RestAssured.
 * </p>
 *
 * @author Denys Ulyanov
 */

public class UpdateIssueApi {


    /**
     * Update state by IID
     * <p>
     * This method handles the call to update issue endpoint
     * </p>
     *
     * @param iid IID of  issue to update
     * @param state New state of the issue
     * @return Response object
     * @see <a href="https://docs.gitlab.com/ee/api/issues.html#edit-issue">Update issue</a>
     */
    public Response updateIssueState(int iid, String state) {

        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());

        return given().log().all()
                .auth().oauth2(Serenity.environmentVariables().getProperty("token"))
                .baseUri(Serenity.environmentVariables().getProperty("base.url"))
                .pathParam("iid", iid)
                .pathParam("state", state)
                .when()
                .put("/issues/{iid}?state_event={state}");
    }
}
