package nl.abnamro.api;

import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import nl.abnamro.model.create.Issue;

import static io.restassured.RestAssured.given;

/**
 * The API representation for New issue at Gitlab Issues API
 * <p>
 * This class handles the calls to create new issue endpoint using RestAssured.
 * </p>
 *
 * @author Denys Ulyanov
 */

public class CreateIssueApi {

    /**
     * Create new issue by title
     * <p>
     * This method handles the call to New Issue endpoint
     * </p>
     *
     * @param title title of new issue
     * @return the Issue object
     * @see <a href="https://docs.gitlab.com/ee/api/issues.html#new-issue">New issue</a>
     */
    public Issue createNewIssue(String title) {

        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());

        return given().log().all()
                .auth().oauth2(Serenity.environmentVariables().getProperty("token"))
                .baseUri(Serenity.environmentVariables().getProperty("base.url"))
                .pathParam("title", title)
                .when()
                .post("/issues?title={title}")
                .as(Issue.class);
    }

    /**
     * RCreate new issue without title
     * <p>
     * This method handles the call to New Issue endpoint without any title
     * </p>
     *
     * @return the Response object
     * @see <a href="https://docs.gitlab.com/ee/api/issues.html#new-issue">New issue</a>
     */
    public Response createIssueWithoutTitle() {

        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());

        return given().log().all()
                .auth().oauth2(Serenity.environmentVariables().getProperty("token"))
                .baseUri(Serenity.environmentVariables().getProperty("base.url"))
                .when()
                .post("/issues?title=");
    }

    /**
     * RCreate new issue without authentication
     * <p>
     * This method handles the call to New Issue endpoint without authentication
     * </p>
     *
     * @return the Response object
     * @see <a href="https://docs.gitlab.com/ee/api/issues.html#new-issue">New issue</a>
     */
    public Response createIssueWithoutAuth() {

        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());

        return given().log().all()
                .baseUri(Serenity.environmentVariables().getProperty("base.url"))
                .pathParam("title", "Some title")
                .when()
                .post("/issues?title={title}");
    }
}
