package nl.abnamro.api;

import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import net.serenitybdd.core.Serenity;
import nl.abnamro.model.search.FoundIssue;

import java.util.List;

import static io.restassured.RestAssured.given;

/**
 * The API representation for list issue at Gitlab Issues API
 * <p>
 * This class handles the calls to list issue endpoint using RestAssured.
 * </p>
 *
 * @author Denys Ulyanov
 */
public class FindIssueApi {

    /**
     * Search issue by title
     * <p>
     * This method handles the call to list issue endpoint
     * </p>
     *
     * @param title Title of  issue to find
     * @return the List<FoundIssue> object
     * @see <a href="https://docs.gitlab.com/ee/api/issues.html#list-issues">List issue</a>
     */
    public List<FoundIssue> findIssueByTitle(String title) {

        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());

        return List.of(given().log().all()
                .auth().oauth2(Serenity.environmentVariables().getProperty("token"))
                .baseUri(Serenity.environmentVariables().getProperty("base.url"))
                .pathParam("title", title)
                .when()
                .get("/issues?search={title}&in=title")
                .as(FoundIssue[].class));
    }

    /**
     * Search issue by IID
     * <p>
     * This method handles the call to list issue endpoint
     * </p>
     *
     * @param iid IID of  issue to find
     * @return the List<FoundIssue> object
     * @see <a href="https://docs.gitlab.com/ee/api/issues.html#list-issues">List issue</a>
     */
    public List<FoundIssue> findIssueByID(int iid) {

        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());

        return List.of(given().log().all()
                .auth().oauth2(Serenity.environmentVariables().getProperty("token"))
                .baseUri(Serenity.environmentVariables().getProperty("base.url"))
                .pathParam("iid", iid)
                .when()
                .get("/issues?iids[]={iid}")
                .as(FoundIssue[].class));
    }
}
