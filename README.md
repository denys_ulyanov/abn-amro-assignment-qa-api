# ABN AMRO ASSIGNMENT QA API  

[Sources][GitLab] | [Build][BuildLink] | [System under test][SATLink] |

## Tech info

The test solution is build with [Serenity](https://serenity-bdd.github.io/) a test
framework and [GitLab](https://about.gitlab.com/) as a CI platform.

## Test description

This solution contains CRUD tests and some corner cases of Gitlab Issues API 

## How to run the tests

There are two ways to run tests:

1) Run tests on GitLab
2) Run tests locally

### Running tests on Gitlab

Visit [Gitlab CI][BuildLink] and hit Run pipeline button.

![Execute test GitLab](./doc/run_gitlab.png)

##### Viewing test results

After tests are executed you can find a report
here: `GitLab > CI/CD > Pipelines > Click on pipeline number > Click run-test stage`
Here you would find detailed execution log and can view HTML report by clicking on Browse button in left panel

![Browse report](./doc/gitlab_browse.png)

Following this pass: `target > site > serenity > index.html` you will be able to see HTML report like this

![Browse report](./doc/report.png)

### Running tests locally

Tests can be executed in an IDE of choice (we prefer Intellij IDEA)

You can execute tests individually from feature files or run all of them by executing `mvn verify`
from IDE terminal

##### Viewing test results

After any test execution you can find report in: `target/site/serenity/index.html`

### Prerequisites

#### Windows

Installation using [chocolatey](https://chocolatey.org/install#installing-chocolatey) and [scoop](https://scoop.sh/):

* Java: `choco install jdk11`
* Maven `choco install maven`

#### Mac

Installation using [Homebrew](https://brew.sh/):

* Java: `brew install java11`
* Java: `brew install maven`

### Project structure

Below you can find locations of the most important bids of the project:

* Features: `src/test/resources/features`
* Step definitions: `src/test/java/starter/stepdefinitions`
* APIs and models: `src/main/java/nl/abnamro`

### Adding new tests

To add a new test you would need to follow this steps:

1) Clone project locally `git clone https://gitlab.com/denys_ulyanov/abn-amro-assignment-qa-api.git`
2) Open project in IDE
2) Create a new branch
3) Add BDD feature to `src/test/resources/features`  describing desired test
4) Implement a step in existing or new step definition `src/test/java/starter/stepdefinitions`
5) Add API calls and models to `src/main/java/nl/abnamro`
7) Run test locally and make sure they are working correctly
8) Push your changes to Gitlab
9) Run all test on GitLab
10) Create a pull request


[GitLab]: https://gitlab.com/denys_ulyanov/abn-amro-assignment-qa-api

[BuildLink]: https://gitlab.com/denys_ulyanov/abn-amro-assignment-qa-api/-/pipelines

[SATLink]: https://docs.gitlab.com/ee/api/issues.html
